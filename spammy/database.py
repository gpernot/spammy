import os.path
import sqlite3
import uuid
from dataclasses import dataclass
from enum import Enum

from cliomatic.output import APIResponse


@dataclass
class Credentials(APIResponse):
    id: int
    server: str
    port: int
    login: str
    password: str


@dataclass
class Sender(APIResponse):
    id: int
    address: str
    smtp: Credentials
    imap: Credentials

    _sender_request = (
        "SELECT sender.id, sender.address,"
        " smtp.id as smtp_id, smtp.server as smtp_server, smtp.port as smtp_port, "
        " smtp.login as smtp_login, smtp.password as smtp_password,"
        " imap.id as imap_id, imap.server as imap_server, imap.port as imap_port, "
        " imap.login as imap_login, imap.password as imap_password"
        " FROM sender"
        " JOIN credentials AS smtp ON sender.smtp_id = smtp.id"
        " JOIN credentials AS imap ON sender.imap_id = imap.id"
    )

    @classmethod
    def from_request(cls, request):
        return cls(
            id=request["id"],
            address=request["address"],
            smtp=Credentials(
                id=request["smtp_id"],
                server=request["smtp_server"],
                port=request["smtp_port"],
                login=request["smtp_login"],
                password=request["smtp_password"],
            ),
            imap=Credentials(
                id=request["imap_id"],
                server=request["imap_server"],
                port=request["imap_port"],
                login=request["imap_login"],
                password=request["imap_password"],
            ),
        )


@dataclass
class Campaign(APIResponse):
    id: int
    title: str
    image: bytes
    content: str
    footnote: str
    contact: str
    sender_id: int


class EmailStatus(Enum):
    WAIT = "wait"
    SENT = "sent"
    RECEIVED = "recv"
    RETRY = "rtry"
    ERROR = "erro"

    def __conform__(self, protocol):
        if protocol is sqlite3.PrepareProtocol:
            return self.value


@dataclass
class Email(APIResponse):
    id: int
    campaign_id: int
    name: str
    address: str
    status: EmailStatus
    envid: str
    diag: str


def dict_factory(cursor, row):
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}


class SpamDB:
    def __init__(self, filename):
        self.db = sqlite3.connect(os.path.expanduser(filename))
        self.db.executescript(
            """
        CREATE TABLE IF NOT EXISTS credentials (
            id INTEGER PRIMARY KEY,
            server TEXT NOT NULL,
            port INT,
            login TEXT NOT NULL,
            password TEXT NOT NULL
            );
            """
        )
        self.db.executescript(
            """
        CREATE TABLE IF NOT EXISTS sender (
            id INTEGER PRIMARY KEY,
            address TEXT NOT NULL UNIQUE,
            smtp_id INTEGER NOT NULL,
            imap_id INTEGER NOT NULL,
            FOREIGN KEY(smtp_id) REFERENCES credentials(id) ON DELETE CASCADE,
            FOREIGN KEY(imap_id) REFERENCES credentials(id) ON DELETE CASCADE
            );
            """
        )
        self.db.executescript(
            """
        CREATE TABLE IF NOT EXISTS campaign (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            image BLOB NOT NULL,
            content TEXT NOT NULL,
            footnote TEXT NOT NULL,
            contact TEXT NOT NULL,
            sender_id INTEGER NOT NULL,
            FOREIGN KEY(sender_id) REFERENCES sender(id) ON DELETE CASCADE
            );
            """
        )
        self.db.executescript(
            """
        PRAGMA foreign_keys = ON;
        CREATE TABLE IF NOT EXISTS campaign_email (
            id INTEGER PRIMARY KEY,
            campaign_id INTEGER NOT NULL,
            status CHAR(4),
            name TEXT,
            address TEXT NOT NULL,
            envid TEXT,
            diag TEXT,
            FOREIGN KEY(campaign_id) REFERENCES campaign(id) ON DELETE CASCADE
            );
        """
        )

    def __del__(self):
        self.db.close()

    def add_credentials(self, server: str, port: int, login: str, password: str) -> int:
        cur = self.db.cursor()
        res = cur.execute(
            "SELECT id from credentials WHERE server = ? AND login = ?",
            (server, login),
        )
        if id_ := res.fetchone():
            return id_[0]
        cur.execute(
            "INSERT INTO credentials (server, port, login, password) VALUES (?, ?, ?, ?)",
            (server, port, login, password),
        )
        res = cur.lastrowid
        self.db.commit()
        return res

    def add_sender(self, address: str, smtp_id: int, imap_id: int) -> int:
        cur = self.db.cursor()
        res = cur.execute("SELECT id from sender WHERE address = ?", (address,))
        if id_ := res.fetchone():
            return id_[0]
        cur.execute(
            "INSERT INTO sender (address, smtp_id, imap_id) VALUES (?, ?, ?)",
            (address, smtp_id, imap_id),
        )
        res = cur.lastrowid
        self.db.commit()
        return res

    def get_sender(self, sender_id: int):
        cur = self.db.cursor()
        cur.row_factory = dict_factory
        cur.execute(f"{Sender._sender_request} WHERE sender.id = ?", (sender_id,))
        return Sender.from_request(cur.fetchone())

    def list_senders(self):
        cur = self.db.cursor()
        cur.row_factory = dict_factory
        cur.execute(Sender._sender_request)
        for sender in cur.fetchall():
            yield Sender.from_request(sender)

    def add_campaign(
        self,
        title: str,
        image: bytes,
        content: str,
        footnote: str,
        contact: str,
        sender_id: int,
    ) -> int:
        cur = self.db.cursor()
        cur.execute(
            "INSERT INTO campaign (title, image, content, footnote, contact, sender_id)"
            "VALUES (?, ?, ?, ?, ?, ?)",
            (title, image, content, footnote, contact, sender_id),
        )
        res = cur.lastrowid
        self.db.commit()
        return res

    def list_campaigns(self):
        cur = self.db.cursor()
        cur.row_factory = dict_factory
        res = cur.execute(
            """SELECT id, title, image, content, footnote, contact, sender_id from campaign"""
        )
        for campaign in res.fetchall():
            yield Campaign(**campaign)

    def get_campaign(self, campaign_id: int):
        cur = self.db.cursor()
        cur.row_factory = dict_factory
        cur.execute("SELECT * FROM campaign WHERE id = ?", (campaign_id,))
        return Campaign(**cur.fetchone())

    def del_campaign(self, campaign_id: int):
        cur = self.db.cursor()
        cur.execute("DELETE FROM campaign WHERE id = ?", (campaign_id,))
        self.db.commit()

    def send_email(self, campaign_id, addresses):
        cur = self.db.cursor()
        cur.executemany(
            "INSERT INTO campaign_email (campaign_id, status, name, address) VALUES (?, ?, ? ,?)",
            [(campaign_id, EmailStatus.WAIT, name, email) for name, email in addresses],
        )
        self.db.commit()

    def campaign_get_emails(self, campaign_id):
        cur = self.db.cursor()
        cur.row_factory = dict_factory
        cur.execute(
            "SELECT * FROM campaign_email WHERE campaign_id = ?", (campaign_id,)
        )
        for email in cur.fetchall():
            email["diag"] = (
                "\\".join(email["diag"].splitlines())[:80] if email["diag"] else None
            )
            yield Email(**email)

    def update_envid(self, email: Email):
        cur = self.db.cursor()
        email.envid = uuid.uuid4().hex
        cur.execute(
            "UPDATE campaign_email SET envid = ? WHERE id = ?",
            (email.envid, email.id),
        )
        self.db.commit()

    def yet_to_process_emails(self, campaign_id: 10, count: int):
        cur = self.db.cursor()
        cur.row_factory = dict_factory
        cur.execute(
            "SELECT id, campaign_id, status, name, address, envid, diag FROM campaign_email WHERE "
            "campaign_id = ? AND status in ('wait', 'retry') ORDER BY RANDOM() LIMIT ?",
            (
                campaign_id,
                count,
            ),
        )
        for email in cur.fetchall():
            yield Email(**email)

    def update_status(self, envid: str | None, status: EmailStatus, diag: str | None):
        if envid is None:
            return
        cur = self.db.cursor()
        cur.execute(
            "UPDATE campaign_email SET status = ?, diag = ? WHERE envid = ?",
            (status, diag, envid),
        )
        self.db.commit()
