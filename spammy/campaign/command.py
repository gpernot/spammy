import csv
import time

import click
import yaml  # type: ignore
from spammy.database import Credentials, Sender, SpamDB

from .mail import receive, render, send
from .utils import load_image


@click.group("campaign")
@click.option(
    "--database",
    "-d",
    help="SQLite3 database file name",
    default="~/.config/spammy.sqlite",
)
@click.pass_obj
def cli(obj, database):
    """Prepare a new campaign"""
    obj.spamdb = SpamDB(database)


@cli.command("add")
@click.argument("campaign", type=click.File("r"))
@click.pass_obj
def campaign_add(obj, campaign):
    """Create a campaign from CAMPAIN YAML file."""
    config = yaml.safe_load(campaign)
    image = load_image(config["image"])
    smtp_id = obj.spamdb.add_credentials(**config["smtp"])
    imap_id = obj.spamdb.add_credentials(**config["imap"])
    sender_id = obj.spamdb.add_sender(
        address=config["address"],
        smtp_id=smtp_id,
        imap_id=imap_id,
    )
    campaign_id = obj.spamdb.add_campaign(
        title=config["title"],
        image=image,
        content=config["content"],
        footnote=config["footnote"],
        contact=config["contact"],
        sender_id=sender_id,
    )
    for listfile in config["lists"]:
        with open(listfile, "r") as csvfile:
            reader = csv.reader(csvfile, dialect="unix")
            try:
                obj.spamdb.send_email(campaign_id, reader)
            except ValueError as exc:
                print(f"Error parsing {csvfile}: {exc}")


@cli.command("list")
@click.pass_obj
def campaign_list(obj):
    """List campaigns."""
    res = obj.spamdb.list_campaigns()
    out = obj.renderer(res, columns=["id", "title", "sender_id"])
    click.echo(out)


@cli.command("del")
@click.argument("campaign_id", type=int)
@click.pass_obj
def campaign_del(obj, campaign_id):
    """Delete campaign CAMPAIGN_ID."""
    obj.spamdb.del_campaign(campaign_id)


@cli.command("send")
@click.argument("campaign_id", type=int)
@click.argument("addresses", type=click.File("r"))
@click.pass_obj
def campaign_send(obj, campaign_id, addresses):
    """Start campaign CAMPAIGN_ID to addresses in CSV file ADDRESSES."""
    reader = csv.reader(addresses, dialect="unix")
    obj.spamdb.send_email(campaign_id, reader)


@cli.command("process")
@click.argument("campaign_id", type=int)
@click.argument("count", type=int)
@click.pass_obj
def campaign_process(obj, campaign_id, count):
    """Process COUNT mail for campaign CAMPAIGN_ID."""
    campaign = obj.spamdb.get_campaign(campaign_id)
    res = obj.spamdb.yet_to_process_emails(campaign_id, count)
    for email in res:
        send(obj.spamdb, obj.spamdb.get_sender(campaign.sender_id), email)


@cli.command("receive")
@click.pass_obj
def campaign_receive(obj):
    """Read DSN from mailbox."""
    for sender in obj.spamdb.list_senders():
        receive(obj.spamdb, sender)


@cli.command("emails")
@click.pass_obj
def campaign_get_emails(obj):
    """Show status of emails."""
    emails = []
    for campaign in obj.spamdb.list_campaigns():
        res = obj.spamdb.campaign_get_emails(campaign.id)
        emails.extend(list(res))
    out = obj.renderer(iter(emails))
    click.echo(out)


@cli.command("cronjob")
@click.argument("batch_size", type=int)
@click.pass_obj
def campaign_cronjob(obj, batch_size):
    """Run cronjob for all campaigns, sending BATCH_SIZE mails for each."""

    # check mailboxes
    for sender in obj.spamdb.list_senders():
        receive(obj.spamdb, sender)

    # process some mails for each campaign
    for campaign in obj.spamdb.list_campaigns():
        emails = obj.spamdb.yet_to_process_emails(campaign.id, batch_size)
        for email in emails:
            send(obj.spamdb, obj.spamdb.get_sender(campaign.sender_id), email)
            time.sleep(8)


def init():
    """Module initialization"""
