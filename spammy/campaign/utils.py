from io import BytesIO

from PIL import Image  # type: ignore

WIDTH = 400


def load_image(filename: str) -> bytes:
    scaled = BytesIO()
    with Image.open(filename) as im:
        ratio = WIDTH / im.width
        resized = im.resize((WIDTH, int(im.height * ratio)))
        resized.save(scaled, format="PNG")
        scaled.flush()
        scaled.seek(0)
    return scaled.read()
