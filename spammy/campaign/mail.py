import imaplib
import smtplib
from email import message_from_bytes
from email.message import EmailMessage
from email.utils import formatdate, make_msgid

import jinja2
from cliomatic.logger import logger
from spammy.database import Campaign, Email, EmailStatus, Sender, SpamDB


def render(spamdb, campaign_id) -> EmailMessage:
    campaign = spamdb.get_campaign(campaign_id)

    message = EmailMessage()
    message["Subject"] = campaign.title

    env = jinja2.Environment(loader=jinja2.PackageLoader("spammy.campaign"))

    txt = env.get_template("mail.txt.j2")
    txt_content = txt.render(
        title=campaign.title,
        content=campaign.content,
        footnote=campaign.footnote,
        contact=campaign.contact,
    )
    message.set_content(txt_content)

    html = env.get_template("mail.html.j2")
    image_cid = make_msgid()
    html_content = html.render(
        title=campaign.title,
        content=campaign.content,
        footnote=campaign.footnote,
        contact=campaign.contact,
        image_cid=image_cid[1:-1],
    )
    message.add_alternative(html_content, subtype="html")
    message.get_payload()[1].add_related(campaign.image, "image", "png", cid=image_cid)

    return message


def send(spamdb: SpamDB, sender: Sender, email: Email):
    # ic(sender)
    spamdb.update_envid(email)
    message: EmailMessage = render(spamdb, email.campaign_id)
    message["From"] = sender.address
    message["To"] = f"{email.name} <{email.address}>"
    message["Reply-To"] = sender.address
    message["Date"] = formatdate(localtime=True)
    server = smtplib.SMTP(sender.smtp.server, port=sender.smtp.port)
    server.starttls()
    server.ehlo_or_helo_if_needed()
    # ic(server.ehlo_resp)
    server.login(sender.smtp.login, sender.smtp.password)
    try:
        server.send_message(
            message,
            from_addr=sender.imap.login,
            mail_options=("RET=HDRS", f"ENVID={email.envid}"),
            rcpt_options=("NOTIFY=SUCCESS,FAILURE",),
        )
    except smtplib.SMTPException as exc:
        spamdb.update_status(
            envid=email.envid,
            status=EmailStatus.ERROR,
            diag=str(exc),
        )
        logger.warning(exc)


def _status_from_dsn(status: str) -> EmailStatus:
    if status.startswith("2."):
        return EmailStatus.RECEIVED
    elif status.startswith("4."):
        return EmailStatus.RETRY
    elif status.startswith("5."):
        return EmailStatus.ERROR
    else:
        raise ValueError


def receive(spamdb: SpamDB, sender: Sender):
    imap = imaplib.IMAP4(sender.imap.server)
    imap.starttls()
    imap.login(sender.imap.login, sender.imap.password)
    imap.select()
    # typ, ids = imap.search(None, "(UNSEEN)")
    typ, ids = imap.search(None, "ALL")
    for num in ids[0].split():
        typ, data = imap.fetch(num, "(RFC822)")
        message = message_from_bytes(data[0][1])
        # typ, data = imap.store(num, "-FLAGS", "\\Seen")
        for part in message.walk():
            if part.get_content_type() == "message/delivery-status":
                status = {}
                for subpart in part.get_payload():
                    status.update(dict(subpart))
                spamdb.update_status(
                    envid=status.get("Original-Envelope-Id", None),
                    status=_status_from_dsn(status["Status"]),
                    diag=status.get("Diagnostic-Code", None),
                )
                typ, data = imap.store(num, "+FLAGS", "\\Deleted")
    imap.expunge()
